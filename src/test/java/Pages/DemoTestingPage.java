package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import java.util.List;

public class DemoTestingPage extends BasePage {

    @FindBy(xpath = "//li[text()=\"First Step\"]/following-sibling::li")
    public List<WebElement> firstTable;
    @FindBy(xpath = "//li[text()=\"Second Step\"]/following-sibling::li")
    public List<WebElement> secondTable;
    @FindBy(xpath = "//li[text()=\"Third Step\"]/following-sibling::li")
    public List<WebElement> thirdTable;


    public DemoTestingPage(WebDriver driver) {
        super(driver);
    }

}
