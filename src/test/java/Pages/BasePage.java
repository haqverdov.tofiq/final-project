package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BasePage {
    WebDriver driver;
    WebDriverWait wait;
    @FindBy(id = "menu-item-2822")
    public WebElement testersHub;
    @FindBy(id = "menu-item-2823")
    public WebElement demoTesting;
    @FindBy(id = "menu-item-2827")
    public WebElement dataPickerPage;
    @FindBy(id = "menu-item-2832")
    public WebElement progressBarPage;


    public BasePage(WebDriver driver) {
        wait = new WebDriverWait(driver, 30);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void moveToDemoTestingPage() {
        Actions actions = new Actions(driver);
        actions.moveToElement(testersHub).perform();
        demoTesting.click();
    }

    public void moveToDatePickerPage() {
        Actions actions = new Actions(driver);
        actions.moveToElement(testersHub).moveToElement(demoTesting).perform();
        dataPickerPage.click();
    }

    public void moveToProgressBarPage() {
        Actions actions = new Actions(driver);
        actions.moveToElement(testersHub).moveToElement(demoTesting).perform();
        progressBarPage.click();
    }

}
