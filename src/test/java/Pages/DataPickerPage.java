package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;

public class DataPickerPage extends BasePage {

    @FindBy(id = "datepicker")
    public WebElement datePickerInput;
    @FindBy(xpath = "//a[@class=\"ui-datepicker-next ui-corner-all\"]")
    public WebElement next;
    @FindBy(xpath = "//a[text()='8']")
    public WebElement anyDay;
    @FindBy(xpath = "//iframe[@class=\"demo-frame lazyloaded\"]")
    public WebElement iframe;

    public DataPickerPage(WebDriver driver) {
        super(driver);
    }

    public void pickDate() {
        driver.switchTo().frame(iframe);
        datePickerInput.click();
        next.click();
        anyDay.click();

    }

    public static boolean isValidFormat(String format, String value, Locale locale) {
        LocalDateTime ldt = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format, locale);

        try {
            ldt = LocalDateTime.parse(value, formatter);
            String result = ldt.format(formatter);
            return result.equals(value);
        } catch (DateTimeParseException e) {
            try {
                LocalDate ld = LocalDate.parse(value, formatter);
                String result = ld.format(formatter);
                return result.equals(value);
            } catch (DateTimeParseException exp) {
                exp.printStackTrace();
            }
        }

        return false;
    }
}
