package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProgressBarPage extends BasePage {
    @FindBy(xpath = "//button[text() = 'Start Download']")
    public WebElement downloadButton;
    @FindBy(xpath = "//div[text()='Complete!']")
    public WebElement completeMessage;
    @FindBy(xpath = "//iframe[@class=\"demo-frame lazyloaded\"]")
    public WebElement iframe;

    public ProgressBarPage(WebDriver driver) {
        super(driver);
    }

    public void clickToDownloadButton() {
        driver.switchTo().frame(iframe);
        downloadButton.click();
    }

    public void waitForDownloading() {
        wait.until(ExpectedConditions.textToBePresentInElement(completeMessage, "Complete!"));
    }
}
