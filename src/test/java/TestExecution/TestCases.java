package TestExecution;

import Pages.DataPickerPage;
import Pages.DemoTestingPage;
import Pages.BasePage;
import Pages.ProgressBarPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TestCases {
    private WebDriver driver;
    private WebDriverWait wait;
    BasePage basePage;
    DataPickerPage dataPicker;
    DemoTestingPage demoTestingPage;
    ProgressBarPage progressBarPage;

    @BeforeMethod
    void setInfo() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 30);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://www.globalsqa.com/");
    }

    @AfterMethod
    void endTesting() {
        driver.quit();
    }

    @Test(priority = 1)
    void checkSixItemsPerColumnInDemoTestingSite() {
        int expectedColumnNumber = 6;
        basePage = new BasePage(driver);
        demoTestingPage = new DemoTestingPage(driver);
        basePage.moveToDemoTestingPage();
        Assert.assertEquals(demoTestingPage.firstTable.size(), expectedColumnNumber);
        Assert.assertEquals(demoTestingPage.secondTable.size(), expectedColumnNumber);
        Assert.assertEquals(demoTestingPage.thirdTable.size(), expectedColumnNumber);
    }

    @Test(priority = 2)
    void checkDateFormatInDatePicker() {
        basePage = new BasePage(driver);
        dataPicker = new DataPickerPage(driver);
        basePage.moveToDatePickerPage();
        dataPicker.pickDate();
        System.out.println(dataPicker.datePickerInput.getAttribute("value"));
        Assert.assertTrue(dataPicker.isValidFormat("MM/dd/yyyy", dataPicker.datePickerInput.getAttribute("value"), Locale.ENGLISH));
    }

    @Test(priority = 3)
    void checkSuccessfulDownloadingInProgressBar() {
        basePage = new BasePage(driver);
        progressBarPage = new ProgressBarPage(driver);
        basePage.moveToProgressBarPage();
        progressBarPage.clickToDownloadButton();
        progressBarPage.waitForDownloading();
        Assert.assertTrue(progressBarPage.completeMessage.isDisplayed());
    }


}
